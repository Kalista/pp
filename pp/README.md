# YoPlate Planner

Implementation of the core algorithm for generation of personal, nutrition optimalized meal plans.

## Quick Start

In order to successfully install the application, these prerequisites must be met:

* Have Git

* Have Python installed

Then it is possible to install the application by following these steps:

1. Clone the repo

    > git clone git@gitlab.com:yoplate/planner.git .

2. Configure the server where the application should run, e.g. create a virtual host.

3. Application is ready to run.

# NOTES
To run the script, where `'user_id'` is a string and saves everithing into local repository under 'user_id'
```bash
python3 meal_plan.py --user_id 'user_id' --results_dir $(pwd)

```

The path to the directory `ray_results` has to be equal to `local_dir` in the training config or be explicitly stated in `--results_dir`
