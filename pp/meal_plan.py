# -*- coding: utf-8 -*-
"""
##Install gym and stable baselines

%%bash
apt-get install -y python-numpy python-dev cmake zlib1g-dev libjpeg- dev xvfb libav-tools xorg-dev python-opengl libboost-all-dev libsdl2-dev swig
pip3 install gym
pip3 install simplejson

##Install RL library

%%bash
pip3 install -U ray
pip3 install setproctitle
pip3 install lz4
pip3 install tensorboardcolab

# Hyperparameters
"""

import pandas as pd
hyperparameters = pd.read_csv("hyperparameters.csv", index_col=[0])
print(hyperparameters)

DAILY_REQ = hyperparameters.at['DAILY_REQ', 'Value'] # 1770
# COEFFICIENT / 100 for DAILY_REQ
COEFFICIENT = hyperparameters.at['COEFFICIENT', 'Value'] # 67
# stop training when REWARD_MEAN is achieved
REWARD_MEAN = hyperparameters.at['REWARD_MEAN', 'Value'] # 50
# raise a error if the training still on after MAX_TIME
MAX_TIME = hyperparameters.at['MAX_TIME', 'Value'] # 2700 # 45 min
# the number of evaluate iterations
ITERATIONS = hyperparameters.at['ITERATIONS', 'Value']
# the number of previously used meals
N_USED_MEALS = hyperparameters.at['N_USED_MEALS', 'Value']

USED_MEALS = [1] * N_USED_MEALS
ENERGY = DAILY_REQ * COEFFICIENT / 100

user = pd.read_csv("user.csv")
meals_table = pd.read_csv("meals.csv")
previous_meals_id = list(meals_table.iloc[:,0])
#print(previous_meals_id)

BREAKFASTS = (user.loc[0,'breakfasts'])/2 #total number of breakfasts divided by two - user has the same breakfast twice
BREAKFASTS = int(BREAKFASTS)
DINNERS = user.loc[0,'dinners'] #same number of dinners and breakfasts
DINNERS = int(DINNERS)
MEALS = DINNERS + BREAKFASTS

"""#Preprocess data from CSVs to generate agregate_set.csv and pantry.csv"""

import numpy as np
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
import time

#Loading CSV files
start_time = time.time()
recepty = pd.read_csv("recipes.csv")
produkty = pd.read_csv("products.csv")
ingredience = pd.read_csv("recipe-ingredients.csv")
parametry = pd.read_csv("ingredients.csv")
old_pantry = pd.read_csv("pantry.csv")
energie_receptu = []
#print(len(old_pantry))

#This section creates a table which will hold recipe ID, nutritional values and product shares in each row. 
baleni = pd.read_csv("products.csv")
column_names = 'recipe'
column_names = np.append(column_names, ['BREAKFAST','DINNER'])
recipe_stat = parametry.columns.values.tolist()[3:]
column_names = np.append(column_names, recipe_stat)
column_names = np.append(column_names, baleni['id'])
podily_baleni = pd.DataFrame(columns = column_names)
pantry = pd.DataFrame(columns = column_names)

# Here we set a value in Kcal. 
# We will scale all the recipes to this energy value.
energie = ENERGY

#Here we go through each recipe and store the amount of each ingredient 
#For each ingredient, we look in the product table and write the package share for this recipe.
for i in range(0,len(recepty)):
  recipe_id = recepty.iloc[i,0]
  podily_baleni.loc[i+1,'recipe'] = recipe_id
  ingredient_amount = ingredience[ingredience['recipe_id'] == recipe_id]
 
  #add the amounts of the same ingredient in one recipe 
  ingredient_amount = ingredient_amount.groupby('ingredient_id')['weight'].sum().reset_index() 
  
  
  """recipe_price = []
  recipe_energy = []
  used_ingreds = []
  ceny_receptu = []
  #count the share price (the actual part of the package we need)
  for e in range(0,len(ingredient_amount)):
    ingredient_id = ingredient_amount.iloc[e,0]
    
    if not (ingredient_id in used_ingreds):
      used_ingreds = np.append(used_ingreds, ingredient_id)
      #preparation for the price and energy calculation
      recipe_amount = ingredient_amount.iloc[e,1]
      package = produkty[produkty['id'] == ingredient_id][['price','weight']]

      #price calculation
      share_price = (recipe_amount/package.iloc[0,1])*package.iloc[0,0]
      recipe_price = np.append(recipe_price, share_price)

      #energy calculation
      ingredient_energy = parametry[parametry['id'] == ingredient_id]['energy']
      share_weight = recipe_amount #/package.iloc[0,0])*package.iloc[0,2]
      share_energy = (share_weight/100)*ingredient_energy
      recipe_energy = np.append(recipe_energy, share_energy)"""
  
  recipe_weight = ingredient_amount.iloc[:,1].sum()
  #print(recipe_weight)
  recipe_ratio = recipe_weight/100
  recipe_energy_100g = recepty.iloc[i,3]
  recipe_energy = recipe_ratio * recipe_energy_100g
  #print(recipe_energy)
  recipe_scale = energie/recipe_energy
  #print(recipe_scale)
  
  
  #ceny_receptu = np.append(ceny_receptu, recipe_price.sum())
  #energie_receptu = np.append(energie_receptu, recipe_energy.sum()) 
  
  
  for e in range(0,len(ingredient_amount)):
    
    #-----------------------------------------------------------------
    #Makes a table with all the packages of products and shares from packages for different recipes.
    #Where we get an ingredient_id match we fill in the share - ie. ingredient-amount_metric/product_amount
    ingredient_id = ingredient_amount.iloc[e,0]
    mnozstvi = ingredient_amount.iloc[e,1]
    baleni = produkty[produkty['id'] == ingredient_id]['weight']
    baleni = baleni.sort_index()
    podil = mnozstvi/baleni
    
    
    scaled_podil = recipe_scale * podil

    podily_baleni.loc[i+1,[ingredient_id]] = scaled_podil.values
    #print(podily_baleni.loc[(i+1),ingredient_id])
    
   
  #print("___________End of one recipe____________")

#---------Konec jednoho receptu-----------------
produkty = produkty.sort_index()
#--------SAME FOR PANTRY-------------
for e in range(0,len(old_pantry)):
  ingredient_id = old_pantry.iloc[e,0]
  mnozstvi = old_pantry.iloc[e,1]
  baleni = produkty[produkty['id'] == ingredient_id]['weight']
  baleni = baleni.sort_index()
  podil = mnozstvi/baleni

  pantry.loc[0,[ingredient_id]] = podil.values

pantry = pantry.fillna(value=0)
#print(pantry)
#pantry.to_csv('vector_pantry.csv')




podily_baleni = podily_baleni.fillna(value=0) 

produkty = produkty.sort_index()

#Calculate the nutritional values of each scaled recipe
for i in range(0,len(podily_baleni['recipe'])):
  
  recipe_stat = recepty.iloc[i,3:]
  #print(recipe_stat)
  scale_by = energie/recipe_stat[0]
  recipe_stat = recipe_stat*scale_by
  
  """recipe_stat = np.zeros(6)
  used_ingredients = []
  for e in range(7,len(podily_baleni.columns.values.tolist())):
    if podily_baleni.iloc[i,e] > 0:
      id_ingredience = podily_baleni.columns.values.tolist()[e]
      
      if not (id_ingredience in used_ingredients):
        used_ingredients = np.append(used_ingredients, id_ingredience)
        ingredient_100g = (produkty.iloc[e-7,5]*podily_baleni.iloc[i,e])/100
        ingredient_stats = parametry[parametry['id'] == id_ingredience]
        ingredient_stats = ingredient_stats.iloc[0,3:9]
        ingredient_stats = ingredient_stats * ingredient_100g
        #print(ingredient_stats.iloc[0])
        recipe_stat = recipe_stat + ingredient_stats"""
  #print(recipe_stat,"Konec receptu")
  
  podily_baleni.iloc[i,3:9] = recipe_stat

  
#print(podily_baleni)    

  
#We mark all the perishable products with negative values to make a distinction *(-1)
#We go through the table and where we find Perishable product column,
#we multiply by (-1)
used_ingredients = []
for e in range(9,len(podily_baleni.columns.values.tolist())):
  id_ingredience = podily_baleni.columns.values.tolist()[e]
  for i in range(0,len(parametry)):
    if parametry.iloc[i,0] == id_ingredience and parametry.iloc[i,2]=='PERISHABLE'and not (id_ingredience in used_ingredients):
      used_ingredients = np.append(used_ingredients, id_ingredience)
      podily_baleni[id_ingredience] = podily_baleni[id_ingredience]*(-1)
      #print(podily_baleni[id_ingredience])

  
#print(podily_baleni)

#generate index list of previously used meals


#EXPORT____________________________  
podily_baleni.to_csv('agregate_set.csv')
pantry.to_csv('vector_pantry.csv')
#files.download('podily.csv')




print("--- %s seconds ---" % (time.time() - start_time))

"""# Pre-filter dataset to breakfast/dinner, select rated recipes based on additional data in recipes.csv"""

import pandas as pd
import numpy as np
from random import sample

recipes = pd.read_csv("recipes.csv")
main_dataset = pd.read_csv("agregate_set.csv", index_col='Unnamed: 0')



#go through recipes - if breakfast - take the id and in main_dataset and add [1,0] at the end 
# we need to mark each recipe if it is dinner or breakfast for the algorithm
for i in range(0, len(recipes)):
  if recipes.iloc[i,2] == 'BREAKFAST':
    main_dataset.iloc[i,[1,2]] = [-1,0]
    #print(main_dataset.iloc[i,[1,2]])
  else:
    main_dataset.iloc[i,[1,2]] = [0,-1]
    
  


#print(main_dataset.sum())

"""
#Take random 50 recipes to decrease the size of the dataset. (Will be base on recipe rating later)
random_selection = sample(range(len(dinner_set)), 50)
random_selection = sorted(random_selection)


#Make sure indexes of recipes in data_selection is the same as recipes
data_selection = dinner_set.iloc[random_selection,]
filtered_recipes = filtered_recipes.iloc[random_selection,]"""

filtered_recipes = recipes
data_selection = main_dataset #dinner_set
#Sve to podily.csv to be used in the algorithm
#print(data_selection)
filtered_recipes.to_csv('recipes_filtered.csv')
data_selection.to_csv('podily.csv')

"""#Meal plan gym environment

Set all global parameters
"""

import math
import gym
from gym import spaces, logger
from gym.utils import seeding
from gym import Env
from gym.spaces import Discrete, Box

#my dependencies
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
import time
import random

#loading data
data = pd.read_csv("podily.csv", index_col='Unnamed: 0')

recepty = pd.read_csv("recipes_filtered.csv")
produkty = pd.read_csv("products.csv")
ingredience = pd.read_csv("recipe-ingredients.csv")
parametry = pd.read_csv("ingredients.csv")
old_pantry = pd.read_csv("vector_pantry.csv", index_col='Unnamed: 0') #leftovers from previous shopping
PricesInTime = [] 

#need a copy of "data" DataFrame with zero values 
#for recipe ID column (whis is now UUID value and cannot enter matrix computations)
#We need the recipe ID in "shop" function
data = data.fillna(0)
recipe_replace = np.zeros(len(data))
data1 = data.copy()
#data1.pop('recipe')
#data1.iloc[:,1] = recipe_replace
data1['recipe'] = recipe_replace


class mealEnv(Env):
  def __init__(self, used_meals = []):
        super(mealEnv, self).__init__()
       
        # Setting action space
        self.action_space = spaces.Discrete(len(data1))
        
        #Setting observation space to values to 1 dimensional vector (row in "data" DataFrame)
        
        self.seed()
        self.meals = MEALS
        self.used_meals = used_meals
        self.meal_counter = self.meals
        self.observation_space = spaces.Box(low=-20,
                                            high=20,
                                            shape=(len(data1.columns) + self.meal_counter + N_USED_MEALS,),
                                            dtype=np.float32)               
        self.action_list = []
        
        
        self.state = np.zeros(len(data1.columns) + self.meal_counter + N_USED_MEALS)
        self.state[MEALS + N_USED_MEALS + 1] = BREAKFASTS
        self.state[MEALS + N_USED_MEALS + 2] = DINNERS
        # starting state with a pantry
        if len(old_pantry) > 0:
          self.state[MEALS + N_USED_MEALS:] += np.array(old_pantry.iloc[0,:]) 
        # add previously consumed meals
        if not self.used_meals:
          self.state[MEALS:MEALS + N_USED_MEALS] = random.sample(range(0, len(data1)), N_USED_MEALS)
        else:
          self.state[MEALS:MEALS + N_USED_MEALS] = self.used_meals

  def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

  def step(self, action): #applies action on the environment
      
      assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))
      
      done = False
      
      #if we find the same recipe in our selection, 
      #we flip the switch and end the episode.
      meal_switch = 0
      if action in self.action_list or action in self.state[MEALS:MEALS + N_USED_MEALS]:
        meal_switch = 1
        reward = -1
        
      elif self.meal_counter > 0:
        self.state[self.meals + N_USED_MEALS:] += np.array(data1.iloc[action,:])
        #print("BEFORE", self.state[:(self.meals + N_USED_MEALS + 6)])
        self.state[self.meals - self.meal_counter] = action
        #print("AFTER", self.state[:(self.meals + N_USED_MEALS + 6)])
        self.action_list = np.append(self.action_list, action)
        self.meal_counter -= 1
        #negative reward if we pick more than assigned number of breakfasts or dinners
        if self.state[self.meals + N_USED_MEALS + 1] < 0 or self.state[self.meals + N_USED_MEALS + 2] < 0: 
          meal_switch = 1
          reward = -1
        else:
          reward = 1
      #update the old state - state is simply list 
      #of groceries with product shares and nutritional values
      #state = self.state
      #print(self.state[:7])
      
      #we keep the list of actions (recipes) to compute the 
      #final list of groceries and price + nutritional values.
      
      
      #print(self.action_list)
      
      # apply action and get new observation
      #simply adding the recipe values to cumulative 
      #episode_values (groceries for all the recipes) -> state
     
      
      #define when the episode is over - once we have 
      #our desired number of meals planned, we are done. 
      #Each step we substitute 1 to arrive at 0 
      
      if self.meal_counter == 0:
        done = True
        #print("HOTOVO")
      
      # count the step reward (we should teach it to return the reward only when done)
      
      if done and meal_switch == 0:
          # Here we call the "shop" method (lower - it computes the final price)
          reward = self.shop(data, self.meals, self.action_list, 10)
          
      #print("REWARD:", reward)
      
      #return Observation, Reward, Done, Info
      return self.state, reward, done, {'actions': self.action_list}

  def reset(self):
      #TO BE DONE - during reset apply previous leftovers as starting state
      #self.state = np.zeros(len(data.columns)) #state array
      #self.state = data1.iloc[self.action_space.sample(),:] #use random leftovers in form of a 1 recipe starting state     
      self.meal_counter = self.meals
      
      self.state = np.zeros(len(data1.columns) + self.meal_counter + N_USED_MEALS)
      self.state[MEALS + N_USED_MEALS + 1] = BREAKFASTS
      self.state[MEALS + N_USED_MEALS + 2] = DINNERS
      # starting state with a pantry
      if len(old_pantry) > 0:
        self.state[MEALS + N_USED_MEALS:] += np.array(old_pantry.iloc[0,:]) 
      # add previously consumed meals
      if not self.used_meals:
        self.state[MEALS:MEALS + N_USED_MEALS] = random.sample(range(0, len(data1)), N_USED_MEALS)
      else:
        self.state[MEALS:MEALS + N_USED_MEALS] = self.used_meals
      
      self.action_list = []
      #self.reward = 0
      #print("RESET")
      return self.state
 
  
  def shop(self, data, meals, action_list, reward):
      
      # Define the States
      Recipe = list(set(data['recipe']))
      leftover_state = []

# ____________________________________Count the final shopping result_____________________________        

      #episode_run = [50,33,48,54,38,46,24,37,42,52] #sample good shopping list
      # we get our recipe list
      episode_run = action_list
      #print(episode_run) #print our recipe indexes

      # Counts the cumulative product amounts again
      nakup = data.iloc[episode_run,9:] # nakup excludes nutritional values!
      nutrition_values = list(data.iloc[episode_run,3:9].sum())
      
      suma = nakup.sum()
      suma.name = 'SUM'
      
      
      if len(old_pantry) != 0:        
        pantry_products = old_pantry.iloc[0,9:] #remove nutrient data
        for p in range(0,len(pantry_products)):
          x = suma[p]
          y = pantry_products[p]
          if x <= 0:
            pass
          else:
            suma[p] = max(0,x-y)


        
      nakup = nakup.append(suma)
      nakup_seznam = nakup
      #print(nakup) 


      # ___________________________Rounding of perishable leftovers___________________
      # Single perishable values are scaled up to 33% up or down 
      #to better fit the package and create minimnum waste
      for i in range(0,len(nakup.columns.values.tolist())):
        if nakup.iloc[meals,i] < 0:
          mnozstvi = abs(nakup.iloc[meals,i])
          zbytek = float(mnozstvi)%(1)
          if zbytek < (float(mnozstvi)/3):

            nakup_seznam.iloc[meals,i]= (-1)*(mnozstvi-zbytek)
          elif (1-zbytek)<(float(mnozstvi)/3):

            nakup_seznam.iloc[meals,i]= (-1)*(mnozstvi+(1-zbytek))                                                                              

      # ___________________________Choosing the best package_____________________   
      #Each ingredient has different packages to choose form, here we pick the best one base on leftovers and price  
      
      # nakup_seznam - table with products and shares       
      # Package - we pick the one closest to the whole number, secondary we use cheaper price.  
      # We only pick package columns with values in them 
      seznam = nakup.loc[:, (nakup != 0).any(axis=0)]
      #print(seznam)

      used_ids=[]
      vyber_potravin=[]
      for i in list(seznam):
        if not (i in used_ids):
          used_ids = np.append(used_ids, i)
          #get indexes of the same products
          same_products = produkty.index[produkty['id']==i].tolist()

          #With indexes, we go pick the package with the lowest 
          #lefover, secondary lowest price, terciary we pick the first in the list. 
          #The result is the final shopping bag with selected product packages and quantities.
          if len(same_products) > 1:
              min_odpad=[]
              for x in same_products:
                if abs(nakup_seznam.iloc[meals,x]//1) > 0:
                  
                  odpad_up = 1 - abs(nakup_seznam.iloc[meals,x])%1
                  odpad = odpad_up #bylo zde min(odpad_up, odpad_down)
                else:
                  odpad = 1 - abs(nakup_seznam.iloc[meals,x])%1


                if min_odpad == []:
                  min_odpad = odpad
                  nej_index = [x]
                elif  min_odpad > odpad:
                  min_odpad = odpad
                  nej_index = [x]
                #print('Monzstvi:',abs(nakup_seznam.iloc[meals,x]),'Odpad',odpad,'min:', min_odpad)

              if len(nej_index) > 1: # if more same size, pick the cheapest
                min_cena = []    
                for y in nej_index:
                  cena = produkty.iloc[y,2] * np.ceil(nakup_seznam.iloc[meals,y]) #price x amount
                  if min_cena == []:
                    min_cena = cena
                    vyber = y
                  elif min_cena > cena:
                    min_cena = cena
                    vyber = y
              else: 
                vyber = nej_index
              vyber_potravin = np.append(vyber_potravin, vyber)
          elif len(same_products) == 1:
            vyber_potravin = np.append(vyber_potravin, same_products)

      #___________________Final tolal price computation,leftovers price and perishable leftovers price___________________    
      vyber_potravin = np.sort(vyber_potravin) # list of used product packages
      #print(vyber_potravin)
      cena_nakupu = [] # final total price
      leftover_price = []
      sum_leftovers_price = []
      perishable_leftover_price = []
      sum_perishable_leftover_price = []
      # compute price perishable lefrovers
      for n in vyber_potravin.astype(int):
        if nakup_seznam.iloc[meals,n] < 0:
          mnozstvi = abs(nakup_seznam.iloc[meals,n])%1

          if mnozstvi > 0:
            mnozstvi = 1-mnozstvi
            package_price = produkty.iloc[n,2]
            #print(produkty.iloc[n,1],'|',mnozstvi)
            perishable_leftover_price = mnozstvi*package_price
            sum_perishable_leftover_price = np.append(sum_perishable_leftover_price, perishable_leftover_price)
      #print('NON Perishable')
      # pcompute price of all the leftovers
      for n in vyber_potravin.astype(int):
        package_quantity = np.ceil(abs(nakup_seznam.iloc[meals,n]) )# Round UP, if there is a leftover, need to buy another whole package

        leftovers = abs(nakup_seznam.iloc[meals,n])%1 # how much of the package we do not use
        if leftovers > 0:
          leftovers = 1-leftovers

          #print(produkty.iloc[n,1],'|',leftovers)
        nakup_seznam.iloc[meals,n] = package_quantity 
        #print(package_quantity)
        package_price = produkty.iloc[n,2]
        #print(package_price)
        cena_packages = package_quantity*package_price
        leftover_price = leftovers*package_price
        sum_leftovers_price = np.append(sum_leftovers_price, leftover_price )
        #print('CENA_________',cena_packages)
        cena_nakupu = np.append(cena_nakupu, cena_packages)
        #print(cena_nakupu)


      #print(sum_perishable_leftover_price)
      if sum_perishable_leftover_price == []:
        #print("Zero perishables")
        sum_perishable_leftover_price = 1 # Best possible score for perishables
      else:
        sum_perishable_leftover_price = sum_perishable_leftover_price.sum()
      celkova_cena_nakupu = cena_nakupu.sum()
      sum_leftovers_price = sum_leftovers_price.sum()


#__________________Print results(total price, cummulative leftovers price, perishable leftovers price)_______________________       

      #print(celkova_cena_nakupu, sum_leftovers_price, sum_perishable_leftover_price)

  
  
  
#__________________COMPUTE REWARD - BASED ON LOWEST PERISHABLE LEFTOVERS AND SECONDARY ON TOTAL PRICE (FOR NOW)_______________________
      #reward for perishable
      if sum_perishable_leftover_price < 300: 
        reward += abs((sum_perishable_leftover_price/10)-30) 
      else:
        reward += -(sum_perishable_leftover_price/20)
      
      #reward for non-perishable leftovers
      """if sum_leftovers_price < 600: 
        reward += abs((sum_leftovers_price/20)-30) 
      else:
        reward += -(sum_leftovers_price/60)"""
      
      #REWARD FOR total price
      """if celkova_cena_nakupu < (MEALS*200): 
        reward += abs((celkova_cena_nakupu/100)-(MEALS*4)) 
      else:
        reward += (-(1)*celkova_cena_nakupu/100)+(MEALS*2)"""
        
      #REWARD FOR PROTEIN CONTENT
      reward += (nutrition_values[3]/(16*MEALS))
       
      
#__________________COMPUTE LEFTOVER STATE TO PASS TO NEXT EPISODE_______________________   

      #TO BE DONE
  
  
#_________________Return the results of the episode_______________________  
      #print(reward)
      return(reward)

#print(old_pantry.head())

"""# User ID"""

"""made as a parser"""

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--user_id", required=True)
parser.add_argument("--results_dir")#, default='/Users/macbook/ray_results')
args = parser.parse_args()
savedir = args.results_dir
user_id = args.user_id

"""#Train

### Run Tensorboard
"""

#import tensorboardcolab as tb
#tbc = tb.TensorBoardColab(graph_path='/root/ray_results/my_experiment')

"""###Register the environment"""

def train_env(renders=False):
    env = mealEnv(used_meals=[])
    return env

from ray.tune.registry import register_env
# register a custom environment
register_env("FoodPlannerTrainer", train_env)

"""###Train"""

import ray
from ray.tune.registry import register_env
import ray.tune as tune
ray.init()

import time

# count the execution time
start = time.time()

# run an experiment with a config
tune.run_experiments({
    user_id: {
        "run": "PPO",
        "env": "FoodPlannerTrainer",
        "stop": {
                  "episode_reward_mean": REWARD_MEAN,
                  "time_total_s": MAX_TIME
        },
        "checkpoint_freq": 10,
        "checkpoint_at_end": True,
        "local_dir": savedir, #"/Users/Documents/Git/Python/Algo",
        #"trial_name_creator": tune.function(custom_name),
        "config": {
            "num_gpus": 0,
            "num_workers": 3,
            "num_envs_per_worker": 1,
            "horizon": MEALS,
            "sample_batch_size": 50,
            "train_batch_size": 2500,
        },
    },
}) #, resume=True)

end = time.time()

# raise a error if the MAX_TIME is reached
if end - start >= MAX_TIME:
  raise TimeoutError

"""# Find the last check point"""

from os import listdir
from os.path import join, isdir
from re import search
from datetime import datetime

# path to the directory
ray_results = args.results_dir

# find the latest agent
mypath = ray_results + '/' + user_id + '/'
onlydirs = [f for f in listdir(mypath) if isdir(join(mypath, f))]

#print(onlydirs)

# read the dates
date = []
for i in onlydirs:
  match = search(r'\d{4}-\d{2}-\d{2}_\d{2}-\d{2}', i)
  date.append(datetime.strptime(match.group(), '%Y-%m-%d_%H-%M'))
  
# find the youngest date
ind = date.index(max(date))

#print(date, "max:", date.index(max(date)))

# append the directory
mypath = mypath + onlydirs[ind]

# find the latest check point
onlydirs = [f for f in listdir(mypath) if isdir(join(mypath, f))]
numbers = [onlydirs[i].split("_")[1] for i in range(len(onlydirs))]
lar_number = max(numbers)

user_path = mypath + '/checkpoint_' + lar_number + '/checkpoint-' + lar_number

print(user_path)

"""#Evaluate model"""

previous_meals = []

def work_env(renders=False):
    env = mealEnv(used_meals=previous_meals)
    return env
  

from ray.tune.registry import register_env
# register a custom environment
register_env("FoodPlanner", work_env)

def init_ppo():
    from ray.rllib.agents import ppo

    config = ppo.DEFAULT_CONFIG.copy()
    config["num_workers"] = 1

    env = work_env()

    agent = ppo.PPOAgent(config=config, env="FoodPlanner")
    agent.restore(user_path)

    return agent, env

def evaluate(_iter = 1):
    """
    :return: a list of (reward, actions) tuples for each iteration
    """

    agent, env = init_ppo()

    all_rewards = []
    
    for _ in range(_iter):
        reward = 0.0
        obs = env.reset()
        done = False
        while not done:
            action = agent.compute_action(obs)
            obs, rew, done, info = env.step(action)
            #print("__STEP__REWARD__", rew, info)
            reward += rew
        all_rewards.append((reward, info.get('actions')))
        #print("Total reward of an iteration:", reward)
    return all_rewards

all_rewards = evaluate(ITERATIONS)

# find the best run out of all the runs
best_run = max(all_rewards, key= lambda item: item[0])

# return the best run as a list of integers
recipes = list(best_run[1].astype(int))

ray.shutdown()

"""# Evaluate shopping bag"""

def shop_eval(recipe_indexes):
        
      recepty = pd.read_csv("recipes_filtered.csv")
      produkty = pd.read_csv("products.csv")
      ingredience = pd.read_csv("recipe-ingredients.csv")
      parametry = pd.read_csv("ingredients.csv")
      old_pantry = pd.read_csv("vector_pantry.csv", index_col='Unnamed: 0') #leftovers from previous shopping
      
      data = pd.read_csv("podily.csv", index_col='Unnamed: 0')
      data = data.fillna(0)
      
      
      Recipe = list(set(data['recipe']))
      leftover_state = []
      meals = len(recipe_indexes)
      

# ____________________________________Count the final shopping result_____________________________        

      # we get our recipe list
      episode_run = recipe_indexes
      #print(episode_run) #print our recipe indexes

      # Counts the cumulative product amounts again
      nakup = data.iloc[episode_run,9:] # nakup excludes nutritional values!
      id_receptu = list(data.iloc[episode_run,0])
      nutrition_values = list(data.iloc[episode_run,3:9].sum())
      print(nutrition_values)
      suma = nakup.sum()
      nazvy_receptu = recepty.iloc[episode_run,[2,3]]
      print(nazvy_receptu)
      # Get leftovers from sum for testing
      sample_leftovers = suma/2
      
      #substract pantry leftovers from shopping
        
      if len(old_pantry) != 0:  
        pantry_products = old_pantry.iloc[0,9:] #remove nutrient data
        for p in range(0,len(pantry_products)):
          if suma[p] < 0:
            pass
          elif pantry_products[p] < suma[p]:
            #print(suma[p], "pantry:", pantry_products[p])
            suma[p] -= pantry_products[p]
            #print(suma[p])
          else:
            #print(suma[p], "pantry:", pantry_products[p])
            suma[p] = 0
            #print(suma[p])
      
        
      
      suma.name = 'SUM'
      nakup = nakup.append(suma)
      nakup_seznam = nakup
      #print(nakup)
      
      
        
       


#_________________________Rounding of perishable leftovers___________________
      # Single perishable values are scaled up to 30% up or down 
      #to better fit the package and create minimnum waste
      for i in range(0,len(nakup.columns.values.tolist())):
        if nakup.iloc[meals,i] < 0:
          mnozstvi = abs(nakup.iloc[meals,i])
          zbytek = float(mnozstvi)%(1)
          if zbytek < (float(mnozstvi)/3):

            nakup_seznam.iloc[meals,i]= (-1)*(mnozstvi-zbytek)
          elif (1-zbytek)<(float(mnozstvi)/3):

            nakup_seznam.iloc[meals,i]= (-1)*(mnozstvi+(1-zbytek))                                                                              
     
      
# ___________________________Choosing the best package_____________________   
      #Each ingredient has different packages to choose form, here we pick the best one base on leftovers and price  
      
      # nakup_seznam - table with products and shares       
      # Package - we pick the one closest to the whole number, secondary we use cheaper price.  
      # We only pick package columns with values in them 
      seznam = nakup.loc[:, (nakup != 0).any(axis=0)]
      

      used_ids=[]
      vyber_potravin=[]
      for i in list(seznam):
        if not (i in used_ids):
          used_ids = np.append(used_ids, i)
          #get indexes of the same products
          same_products = produkty.index[produkty['id']==i].tolist()
          
          #With indexes, we go pick the package with the lowest 
          #lefover, secondary lowest price, terciary we pick the first in the list. 
          #The result is the final shopping bag with selected product packages and quantities.
          if len(same_products) > 1:
              min_odpad=[]
              for x in same_products:
                if abs(nakup_seznam.iloc[meals,x]//1) > 0:
                  
                  odpad_up = 1 - abs(nakup_seznam.iloc[meals,x])%1
                  odpad = odpad_up #bylo zde min(odpad_up, odpad_down)
                else:
                  odpad = 1 - abs(nakup_seznam.iloc[meals,x])%1


                if min_odpad == []:
                  min_odpad = odpad
                  nej_index = [x]
                elif  min_odpad > odpad:
                  min_odpad = odpad
                  nej_index = [x]
                #print('Monzstvi:',abs(nakup_seznam.iloc[meals,x]),'Odpad',odpad,'min:', min_odpad)

              if len(nej_index) > 1: # if more same size, pick the cheapest
                min_cena = []    
                for y in nej_index:
                  cena = produkty.iloc[y,2] * np.ceil(nakup_seznam.iloc[meals,y]) #price x amount
                  if min_cena == []:
                    min_cena = cena
                    vyber = y
                  elif min_cena > cena:
                    min_cena = cena
                    vyber = y
              else: 
                vyber = nej_index
              vyber_potravin = np.append(vyber_potravin, vyber)
          elif len(same_products) == 1:
            vyber_potravin = np.append(vyber_potravin, same_products)
      

#_____________GET SCALED INGREDIENT WEIGHTS FOR MELAPLAN EXPORT___________________
      # Get scaling ratio in perishable goods and multiply the package shares 
      seznam = nakup.loc[:, (nakup != 0).any(axis=0)] #seznam contains only columns, that are used in this shopping bag
      ratio = nakup_seznam.iloc[meals,:]/suma
      ratio = ratio.fillna(1)
      recipes = data.iloc[episode_run,9:]
      
      mealPlan = []
      for i in range(0,meals):
        scaled_recipe = recipes.iloc[i,:]*ratio
        #print(scaled_recipe)
        used_ids=[]
        ingredients = []
        
        for e in list(seznam): 
          if not (e in used_ids):
            used_ids = np.append(used_ids, e)
            #get indexes of the same products
            same_products = produkty.index[produkty['id']==e].tolist()

            #With indexes, we go pick the first package with the lowest 

            if len(same_products) >= 1:
              product_index = same_products[0] 
              #use absolute value to list weight of perishables in positive numbers
              new_value = abs(produkty.loc[product_index,'weight']*scaled_recipe[product_index]) 
              #scaled_recipe[product_index] = new_value
              if new_value != 0:
                json = {"id": produkty.loc[product_index,'id'], "weight": new_value}
                ingredients.append(json)
            else:
              pass
              #print('CHYBA - NENÍ INDEX V PRODUKTECH')
              
        #export prep      
        #mealType = recepty[recepty['id']==id_receptu[i]]['meal']     
        mealType = recepty.loc[episode_run[i],'meal']  
        servings = 2 #servings per scaled recipe
        recipe = {"servings": servings,"meal": mealType,"id": id_receptu[i],"ingredients":ingredients}
        mealPlan.append(recipe) 
        #print(mealPlan)
      export = {"mealPlan":mealPlan}
      #print(export)                                                                             
      
      
#___________________Final tolal price computation, PANTRY, leftovers price and perishable leftovers price___________________    
      vyber_potravin = np.sort(vyber_potravin) # list of used product packages
      #print(vyber_potravin)
    
      cena_nakupu = [] # final total price
      leftover_price = []
      sum_leftovers_price = []
      perishable_leftover_price = []
      sum_perishable_leftover_price = []
      # compute price perishable lefrovers
      for n in vyber_potravin.astype(int):
        if nakup_seznam.iloc[meals,n] < 0:
          mnozstvi = abs(nakup_seznam.iloc[meals,n])%1

          if mnozstvi > 0:
            mnozstvi = 1-mnozstvi
            package_price = produkty.iloc[n,2]
            #print(produkty.iloc[n,1],'|',mnozstvi)
            perishable_leftover_price = mnozstvi*package_price
            sum_perishable_leftover_price = np.append(sum_perishable_leftover_price, perishable_leftover_price)
      
      
      #PANTRY computation
      leftovers = []
      ingredient_ids = []
      ingredient_weights = []
      
      for n in vyber_potravin.astype(int):
        if nakup_seznam.iloc[meals,n] > 0: # do not include perishable ingredients to pantry
          package_quantity = np.ceil(nakup_seznam.iloc[meals,n]) # Round UP, if there is a leftover, need to buy another whole extra package

          leftovers = nakup_seznam.iloc[meals,n]%1 # how much of the package we do not use
          if leftovers > 0:
            leftovers = 1-leftovers

            #print(produkty.iloc[n,1],'|',leftovers)
            
          package_weight = produkty.iloc[n,5]
          ingredient_weight = leftovers * package_weight
          ingredient_weights.append(ingredient_weight)
          ingredient_id = produkty.iloc[n,0]
          ingredient_ids.append(ingredient_id)
          
      #print(ingredient_id,'|',ingredient_weight)
      pantry_dict = {"ingredient_id": ingredient_ids, "weight": ingredient_weights}
      pantry = pd.DataFrame(data=pantry_dict)
      #print(pantry)
         
            
      # compute and price of all the leftovers
      for n in vyber_potravin.astype(int):
        package_quantity = np.ceil(abs(nakup_seznam.iloc[meals,n]) )# Round UP, if there is a leftover, need to buy another whole package

        leftovers = abs(nakup_seznam.iloc[meals,n])%1 # how much of the package we do not use
        if leftovers > 0:
          leftovers = 1-leftovers

          #print(produkty.iloc[n,1],'|',leftovers)
        nakup_seznam.iloc[meals,n] = package_quantity 
        #print(package_quantity)
        package_price = produkty.iloc[n,2]
        #print(package_price)
        cena_packages = package_quantity*package_price
        leftover_price = leftovers*package_price
        sum_leftovers_price = np.append(sum_leftovers_price, leftover_price )
        #print('CENA_________',cena_packages)
        cena_nakupu = np.append(cena_nakupu, cena_packages)
        #print(cena_nakupu)
        
 
            
      #print(sum_perishable_leftover_price)
      if sum_perishable_leftover_price == []:
        #print("Zero perishables")
        sum_perishable_leftover_price = 1 # Best possible score for perishables
      else:
        sum_perishable_leftover_price = sum_perishable_leftover_price.sum()
      celkova_cena_nakupu = cena_nakupu.sum()
      sum_leftovers_price = sum_leftovers_price.sum()
      

#__________________Print results(total price, cummulative leftovers price, perishable leftovers price)_______________________       

      print(celkova_cena_nakupu, sum_leftovers_price, sum_perishable_leftover_price)
  

#__________________COMPUTE LEFTOVER STATE TO PASS TO NEXT EPISODE_______________________   

      #pantry.to_csv('pantry.csv', index=False)
  
  
#_________________Return the results of the episode_______________________  
      #print(reward)
      return(export)

import pandas as pd
import numpy as np
#[ 25,  91,  96,  65,  46,  23, 118,   1, 105,  60,]

#recipes = [ 25,  91,  21,  73, 105,   2,  92,  11,  46,  26]
shop_eval(recipes)
#print(recepty.iloc[recipes])

"""# Meal plan ingredients to Json export"""

import simplejson as json

#get dictionary of mealPlan into "export"
recipes = recipes
export = shop_eval(recipes)

#convert dictionary to json and export .json file
with open("mealPlan.json", 'w') as f:
  json.dump(export, f, sort_keys=True, indent=4)

        
        
"""JSON STRUCTURE
{
    "mealPlan": [
        {
            "id": "id receptu",
            "ingredients": [
                {
                    "id": "id ingredience",
                    "weight": "naškálovaná váha ingredience v receptu"
                }
            'meal': 'DINNER',
            'servings': 2},
            ]
        }
    ]
}
"""